# ABP Caps Limit

Restrict usage of capitals in thread titles

## Installation / Upgrade
- Upload the content of the Upload/ directory to your forum root
- Install & activate the plugin
- Change settings

## Settings
- **Active forum** : the plugin can work on all forums or just the ones you want
- **Excluded groups** : some usergroups can use capitals without moderation
- **Minimal title length** : if title length is less than the defined value, it won't be checked
- **Ratio** : you define the proportion of allowed capitals
- **Action** : block the thread or lowercase it 

## TODO
Make the plugin working with thread edition
