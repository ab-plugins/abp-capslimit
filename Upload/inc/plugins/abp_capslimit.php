<?php
/**
 * Restrict usage of capitals in thread titles
 * Copyright 2022 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPCAPS', str_replace('.php', '', basename(__FILE__)));

function abp_capslimit_info() {
	global $lang;
	$lang->load(CN_ABPCAPS);
	return [
		'name' => $lang->abp_capslimit_name,
		'description' => $lang->abp_capslimit_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
		'website' => 'https://gitlab.com/ab-plugins/abp-capslimit',
		'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '0.2',
        'compatibility' => '18*',
        'codename' => CN_ABPCAPS
	];
}

function abp_capslimit_install() {
	global $db, $lang;
	$lang->load(CN_ABPCAPS);
	$settinggroups = [
		'name' => CN_ABPCAPS,
		'title' => $lang->abp_capslimit_setting_title,
		'description' => $lang->abp_capslimit_setting_desc,
		'disporder' => 0,
		'isdefault' => 0
	];
	$db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
	$settings = [
		[
			'gid' => $gid,
			'name' => CN_ABPCAPS . '_forums',
            'title' => $lang->abp_capslimit_forums,
            'description' => $lang->abp_capslimit_forums_desc,
            'optionscode' => 'forumselect',
            'value' => -1,
            'disporder' => 1
		],
		[
			'gid' => $gid,
			'name' => CN_ABPCAPS . '_gexcluded',
            'title' => $lang->abp_capslimit_gexcluded,
            'description' => $lang->abp_capslimit_gexcluded_desc,
            'optionscode' => 'groupselect',
            'value' => '3,4,6',
            'disporder' => 2
		],
		[
			'gid' => $gid,
			'name' => CN_ABPCAPS . '_tsize',
            'title' => $lang->abp_capslimit_tsize,
            'description' => $lang->abp_capslimit_tsize_desc,
            'optionscode' => 'numeric'.PHP_EOL.'min=5',
            'value' => 5,
            'disporder' => 3
		],
		[
			'gid' => $gid,
			'name' => CN_ABPCAPS . '_ratio',
            'title' => $lang->abp_capslimit_ratio,
            'description' => $lang->abp_capslimit_ratio_desc,
            'optionscode' => 'numeric'.PHP_EOL.'min=10'.PHP_EOL.'max=100'.PHP_EOL.'step=5',
            'value' => 10,
            'disporder' => 4
		],
		[
			'gid' => $gid,
			'name' => CN_ABPCAPS . '_action',
            'title' => $lang->abp_capslimit_action,
            'description' => $lang->abp_capslimit_action_desc,
            'optionscode' => 'select'.PHP_EOL.'0='.$lang->abp_capslimit_action_block.PHP_EOL.'1='.$lang->abp_capslimit_action_lower.PHP_EOL,
            'value' => 0,
            'disporder' => 5
		]
	];
	$db->insert_query_multiple('settings', $settings);
    rebuild_settings();
}

function abp_capslimit_is_installed() {
	global $mybb;
	return (array_key_exists(CN_ABPCAPS . '_action', $mybb->settings));
}

function abp_capslimit_uninstall() {
	global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPCAPS . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPCAPS . "'");
    rebuild_settings();
}

// Works on new thread
$plugins->add_hook('datahandler_post_validate_thread', 'abp_capslimit_checktitle');
function abp_capslimit_checktitle(&$datahandler) {
	global $mybb, $lang;
	if (isset($datahandler->data['edit_uid']) && (int)$datahandler->data['edit_uid'] != 0) {
        $puser = get_user($datahandler->data['edit_uid']);
    } else {
        $puser = get_user($datahandler->data['uid']);
    }
    if (is_member($mybb->settings[CN_ABPCAPS . '_gexcluded'], $puser['uid'])) {
        return;
    }
	$forums = explode(',', $mybb->settings[CN_ABPCAPS.'_forums']);
	if ($forums[0]!=-1 && !in_array($datahandler->data['fid'], $forums)) {
		return;
	}
	$ititle = $datahandler->data['subject'];
	if (strlen($ititle)<$mybb->settings[CN_ABPCAPS.'_tsize']) {
		return;
	}
	$caps = strlen(trim(preg_replace('/[^[:upper:]]/', '', $ititle)));
	if ($caps>0 && (100*$caps/strlen($ititle))>$mybb->settings[CN_ABPCAPS.'_ratio']) {
		switch ($mybb->settings[CN_ABPCAPS . '_action']) {
			case 1:
				$datahandler->data['subject'] = strtolower($datahandler->data['subject']);
				break;
			default:
				$lang->load(CN_ABPCAPS);
				$datahandler->set_error($lang->sprintf($lang->abp_capslimit_block, $mybb->settings[CN_ABPCAPS.'_ratio']));
				break;
		}
	}
	return;
}