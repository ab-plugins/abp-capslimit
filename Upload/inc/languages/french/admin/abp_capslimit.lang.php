<?php
/**
 * Restrict usage of capitals in thread titles
 * Copyright 2022 CrazyCat <crazycat@c-p-f.org>
 */
 $l['abp_capslimit_name'] = 'ABP Capitals limiter';
 $l['abp_capslimit_desc'] = 'Limite l\'utilisation des majuscules dans les titres';
 
 $l['abp_capslimit_setting_title'] = 'Réglages pour ABP Capital limiter';
 $l['abp_capslimit_setting_desc'] = 'Petites options d\'affinage';
 $l['abp_capslimit_forums'] = 'Forum actifs';
 $l['abp_capslimit_forums_desc'] = 'Forums où le limiteur est actif';
 $l['abp_capslimit_gexcluded'] = 'Groupes exclus';
 $l['abp_capslimit_gexcluded_desc'] = 'Groupes d\'utilisateurs qui ne sont pas affectés par la limitation';
 $l['abp_capslimit_tsize'] = 'Longueur minimale du titre';
 $l['abp_capslimit_tsize_desc'] = 'En dessous de cette longueur, la limite n\'est pas appliquée';
 $l['abp_capslimit_ratio'] = 'Ratio';
 $l['abp_capslimit_ratio_desc'] = 'Ratio maximal (en %) de majuscules autoriées';
 $l['abp_capslimit_action'] = 'Action';
 $l['abp_capslimit_action_desc'] = 'Action appliquée lorsqu\'il y a trop de majuscules';
 $l['abp_capslimit_action_block'] = 'Bloquage du sujet';
 $l['abp_capslimit_action_lower'] = 'Passage en minuscules';