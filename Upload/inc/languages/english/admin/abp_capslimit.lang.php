<?php
/**
 * Restrict usage of capitals in thread titles
 * Copyright 2022 CrazyCat <crazycat@c-p-f.org>
 */
 $l['abp_capslimit_name'] = 'ABP Capitals limiter';
 $l['abp_capslimit_desc'] = 'Restrict usage of capitals in thread titles';
 
 $l['abp_capslimit_setting_title'] = 'ABP Capitals limiter settings';
 $l['abp_capslimit_setting_desc'] = 'Short options to tune the limiter';
 $l['abp_capslimit_forums'] = 'Active forums';
 $l['abp_capslimit_forums_desc'] = 'Forums where the limiter is active';
 $l['abp_capslimit_gexcluded'] = 'Excluded groups';
 $l['abp_capslimit_gexcluded_desc'] = 'User groups non affected by limitation';
 $l['abp_capslimit_tsize'] = 'Minimal title length';
 $l['abp_capslimit_tsize_desc'] = 'Under this length, no check will be done';
 $l['abp_capslimit_ratio'] = 'Ratio';
 $l['abp_capslimit_ratio_desc'] = 'Maximal ratio (in %) of allowed capitals in title';
 $l['abp_capslimit_action'] = 'Action';
 $l['abp_capslimit_action_desc'] = 'Action applicated when too much capitals are in the title';
 $l['abp_capslimit_action_block'] = 'Block posting';
 $l['abp_capslimit_action_lower'] = 'Lowercase title';